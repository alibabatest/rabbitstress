## [1.0.1](https://gitlab.com/alibabatest/rabbitstress/compare/1.0.0...1.0.1) (2023-11-01)


### Bug Fixes

* add forever sleep ([492e81b](https://gitlab.com/alibabatest/rabbitstress/commit/492e81b8dea6bebb4cc929d03ba9352f3d02dcc5))

# 1.0.0 (2023-11-01)


### Bug Fixes

* change quotations ([35c301d](https://gitlab.com/alibabatest/rabbitstress/commit/35c301dcecd70334785f61c8f2f0df186c438d52))


### Features

* init project ([e956bbf](https://gitlab.com/alibabatest/rabbitstress/commit/e956bbf96a70de0e7b0d54214b1ee48c17efeaa9))
