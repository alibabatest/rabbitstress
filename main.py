import os
import pika
import time

time.sleep(10000000)

rabbitmq_url = os.environ["RABBITMQ_URL"]

connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_url))
channel = connection.channel()

while True:
    channel.basic_publish(exchange="", routing_key="test", body="Hello!")
    print(" [x] Sent message!")
    time.sleep(0.5)
